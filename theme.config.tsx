import React from "react";
import { DocsThemeConfig } from "nextra-theme-docs";
import { useRouter } from "next/router";

const config: DocsThemeConfig = {
  logo: <span>Arctibyte Documentation</span>,
  chat: {
    link: "https://discord.com",
  },
  docsRepositoryBase: "https://gitlab.com/vonpenitzel/docs",
  footer: {
    text: "Magio and Ignis Documentation",
  },
  useNextSeoProps() {
    const { asPath } = useRouter();
    if (asPath !== "/") {
      return {
        titleTemplate: "%s – SWR",
      };
    }
  },
};

export default config;
